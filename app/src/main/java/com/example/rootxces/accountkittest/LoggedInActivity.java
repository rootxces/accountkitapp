package com.example.rootxces.accountkittest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.PhoneNumber;
import com.facebook.login.LoginManager;

public class LoggedInActivity extends AppCompatActivity {
    Button buttonLogout;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logged_in);

        textView = findViewById(R.id.text);
        AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
            @Override
            public void onSuccess(Account account) {
                String accountId = account.getId();
                PhoneNumber phoneNumber = account.getPhoneNumber();
                if(account.getPhoneNumber()!=null){
                    String formattedPhoneNumber = phoneNumber.toString();
                    textView.setText("ID is: "+accountId+" Phone No. is: "+formattedPhoneNumber);
                }else{
                    String emailString = account.getEmail();
                    textView.setText("ID is: "+accountId+" Email is: "+emailString);
                }
            }

            @Override
            public void onError(AccountKitError accountKitError) {

            }
        });
        buttonLogout = findViewById(R.id.btn_logout);
        buttonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AccountKit.logOut();
                LoginManager.getInstance().logOut();
                goToMainActivity();
            }
        });
    }

    public void goToMainActivity(){
        Intent i  = new Intent(this,MainActivity.class);
        startActivity(i);
        finish();
    }
}
